$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.p-map__list > li > a').on('click', function (e) {
    e.preventDefault();

    $('.p-map-modal').hide();
    $(this).next().toggle();
  });

  $('.p-map-modal__close').on('click', function (e) {
    e.preventDefault();

    $('.p-map-modal').hide();
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.p-modal').toggle();
  });

  $('.p-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'p-modal__centered') {
      $('.p-modal').hide();
    }
  });

  $('.p-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.p-modal').hide();
  });

  new Swiper('.p-reviews__cards', {
    centeredSlides: true,
    loop: true,
    spaceBetween: 100,
    slidesPerView: 'auto',
    allowTouchMove: false,
    navigation: {
      nextEl: '.p-reviews__cards .swiper-button-next',
      prevEl: '.p-reviews__cards .swiper-button-prev',
    },
  });

  new Swiper('#done', {
    centeredSlides: true,
    loop: true,
    spaceBetween: 16,
    slidesPerView: 'auto',
    allowTouchMove: false,
    autoplay: {
      delay: 0,
    },
    loopedSlides: 12,
    freeMode: true,
    speed: 2000,
    breakpoints: {
      1200: {
        centeredSlides: true,
        loop: true,
      },
    },
  });

  new Swiper('#prof', {
    centeredSlides: true,
    loop: true,
    spaceBetween: 16,
    slidesPerView: 'auto',
    allowTouchMove: false,
    autoplay: {
      delay: 0,
    },
    loopedSlides: 12,
    freeMode: true,
    speed: 2000,
    breakpoints: {
      1200: {
        centeredSlides: true,
        loop: true,
      },
    },
  });

  new Swiper('.p-team__cards', {
    loop: true,
    spaceBetween: 50,
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.p-team__cards .swiper-button-next',
      prevEl: '.p-team__cards .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        loop: true,
      },
    },
  });

  $(function () {
    new WOW().init();
  });

  $(window).on('load', function () {
    if ($(window).width() < 1200) {
      $('.p-header__city').appendTo('.p-header__right');
      $('.p-top__text').appendTo('.p-top__title');

      new Swiper('.p-prices__cards', {
        centeredSlides: true,
        loop: true,
        spaceBetween: 40,
        slidesPerView: 'auto',
        pagination: {
          el: '.p-prices__cards .swiper-pagination',
          dynamicBullets: true,
        },
      });

      $('.p-map__scroll').each(element, new SimpleBar());

      /* $("#about_replace_2").detach().insertAfter("#about_replace_to"); */
    }
  });

  $(window).on('load scroll', function () {
    var parallaxElement = $('.parallax_scroll'),
      parallaxQuantity = parallaxElement.length;
    window.requestAnimationFrame(function () {
      for (var i = 0; i < parallaxQuantity; i++) {
        var currentElement = parallaxElement.eq(i),
          windowTop = $(window).scrollTop(),
          elementTop = currentElement.offset().top,
          elementHeight = currentElement.height(),
          viewPortHeight = window.innerHeight * 0.5 - elementHeight * 0.5,
          scrolled = windowTop - elementTop + viewPortHeight;
        currentElement.css({
          transform: 'translate3d(0,' + scrolled * -0.15 + 'px, 0)',
        });
      }
    });
  });
});

$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};
